# Sofka Training Test

## Ejercicio 1

### Determine el valor de un pasaje de avión, conociendo la distancia a recorrer, el número de días de estancia, y sabiendo que, si la distancia a recorrer es superior a 1000 km. y el número de días de estancia es superior a 7, la línea aérea le hace descuento del 30%. (El precio por km. Es $35,00).

## Ejercicio 2

### Un BOING 747 tiene una capacidad de carga para equipaje de aproximadamente 18.000 kg. Confeccione un algoritmo que controle la recepción de equipajes para este avión, sabiendo:
### •	Un bulto no puede exceder la capacidad de carga del avión ni tampoco exceder los 500 kg.
### •	El valor por kilo del bulto es:
### - De 0 a 25 kg. Cero pesos.
### - De 26 a 300 kg. 1500 pesos por kilo de equipaje.
### - De 301 a 500 kg. 2500 pesos por kilo de equipaje.

### Para un vuelo cualquiera se pide:
### a)	Número total de bultos ingresados para el vuelo.
### b)	Peso del bulto más pesado y del más liviano.
### c)	Peso promedio de los bultos.
### d)	Ingreso en pesos y en dólares por concepto de carga.

### Construya una tabla de seguimiento con no menos de 15 bultos para realizar la prueba del algoritmo.



