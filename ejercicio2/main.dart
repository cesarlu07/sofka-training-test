import 'dart:math';

main(List<String> args)
{
  List<double> luggage = [];

  Random randomLuggageWeight = Random();
  double sumLuggageWeight = 0, luggagePrice = 0, dollar = 31/100000;
  const maxWeihtCapacity = 18000, maxLuggageWeight = 500;
  int counter = 0;

  print('\n');
  print('---------------------------------------------------------------------------------');
  
  print('\t\t Control Layout of BOING 747 Luggage Capacity');
  print('---------------------------------------------------------------------------------\n');
  print('Number \t Luggage Weight \t Luggage Price (pesos) \t Luggage Price (dollars)');
  print('---------------------------------------------------------------------------------');
  while(sumLuggageWeight < maxWeihtCapacity)
  {
    luggage.add(randomLuggageWeight.nextDouble()*maxLuggageWeight);
    luggagePrice = luggagePrice + rate(luggage[counter], luggagePrice);
    sumLuggageWeight = luggage[counter] + sumLuggageWeight;
    print('${counter+1} \t ${luggage[counter]} \t ${rate(luggage[counter], luggagePrice)} \t ${(rate(luggage[counter], luggagePrice))*dollar}');
    print('---------------------------------------------------------------------------------');
    counter++;
  }
  print('\n');
  print("Summatory of the Luggage's weight: ${sumLuggageWeight.toStringAsFixed(2)}\n");
  print('Total number of checked luggage: ${counter}\n');
  print('Maximum luggage weight: ${luggage.reduce(max).toStringAsFixed(2)}\n');
  print('Minimum luggage weight: ${luggage.reduce(min).toStringAsFixed(2)}\n');
  print('Promedium luggage weight: ${(sumLuggageWeight/counter).toStringAsFixed(2)}');
  print('\n');
}

double rate(double luggage, double price)
{
  const double priceA = 1500, priceB = 2500, minWeight = 25, midleWeight = 300;
  
  if(luggage <= minWeight)
    {
      price = price*0;
    }
    else if(luggage > minWeight && luggage <= midleWeight)
    {
      price = priceA*(luggage-minWeight);
    }
    else
    {
      price = priceB*(luggage-minWeight);
    }
    return price;
}